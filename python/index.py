from abc import ABCMeta, abstractmethod
import cvs
class Projects(object):
    """Base Class for holding a list of projects"""
    __metaclass__ = ABCMeta
    
    def __init__(self, projects=None):
        self.projects = projects or []

    @abstractmethod
    def ListType():
        """Return the project list Class"""
        pass
         
    @absstractmethod
    def LoadCSVData(self):
        """Load CSV file into objects """
        pass
    
    
    @abstractmethod
    def DictionaryKeys(self):
        """return a list of deictionary keys"""
        pass
class pipeline(projects):
    """Class for Pipeline table"""
    
    def ListType():
        """class type"""
        return "pipeline"
    
class project(object):
    """Base Class for Projects"""
    
    __metaclass__ = ABCMeta
    
    
    def __init__(self,
                 pid=None,
                 project_name=None,
                 customer=None,
                 project_type=None,
                 location=None,
                 remote=None,
                 sow_signed=None,
                 sales=None,
                 pri=None,
                 sec=None,
                 projected_start=None,
                 projected_end=None,
                 invoiced=None):
        self.pid = pid
        self.project_name = project_name
        self.customer = customer
        self.project_type = project_type
        self.location = location
        self.remote = remote
        self.sow_signed = sow_signed
        self.sales = sales
        self.pri = pri
        self.sec = sec
        self.projected_start = projected_start
        self.projedcted_end = projected_end
        self.invoiced = invoiced
    

    @abstractmethod
    def ListType():
        """Return the project Class"""
        pass
    
    
    @abstractmethod
    def TableRow():
        """return opject as a html table row"""
        pass
    
    
class pipelineproject(project):
    """pipe line type projects"""
    
    def ListType():
        return "Pipeline V1"
    
    def TableRow(self, taboffset=8):
        lineoffset = chr(32) * taboffset
        startofrow = """%s<tr class="project">%s""" % (lineoffset,chr(10))
        endofrow = """%s</tr>%s""" % (lineoffset, chr(13))
        rowdata = """%s    <td class="project">%s</td>%s""" % (lineoffset, self.pid, chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.project_name,chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.customer,chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.project_type,chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.location,chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.remote,chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.sales,chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.pri,chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.sec,chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.projected_start,chr(10))
        rowdata = """%s%s    <td class="project">%s</td>%s""" % (rowdata, lineoffset, self.projedcted_end,chr(10))
        row = """%s%s%s""" % (startofrow, rowdata, endofrow) 
        return row
        
if __name__ == "__main__":
    p = pipeline(pid=3,
                 project_name='Project test',
                 customer='Test mfg, inc',
                 project_type='ACH - onsite',
                 location="Rochester",
                 remote='Yes',
                 sow_signed='01/01/2016',
                 sales='Candy',
                 pri='Harold',
                 sec='Unassigned',
                 projected_start='01/14/2016',
                 projected_end='01/17/2016',
                 invoiced='F')
    print p.TableRow()