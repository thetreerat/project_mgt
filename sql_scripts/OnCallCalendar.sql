\c project_board
create table calendar (
    caid serial PRIMARY KEY,
    calendar_name varchar(30),
    );
create table calendar_template (
    tid serial PRIMARY KEY,
    caid integer REFERENCES calendar(caid),
    shift_name varchar(30),
    shift_start_time time,
    shift_end_time time,
    );
create table shifts (
    sid serial primary key,
    tid integer REFERENCES calendar_template(eid),
    caid integer REFERENCES calendar(eid),
    primary_resource integer REFERENCES employee(eid),
    backup_resource integer REFERENCES employee(eid),
    shift_start_date as date,
    shift_end_date as date
    );
create table calendar_workers (
    cwid serial primary key,
    eid integer REFERENCES employee(eid);

create table calendar_editors (
    ceid serial primary key,
    eid integer  REFERENCES employee(eid)
    );