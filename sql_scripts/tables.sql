create database project_board;
\c project_board
create table employee (
    EID serial PRIMARY KEY,
    first_name varchar(30),
    last_name varchar(30),
    email varchar(50),
    phone1 varchar(15),
    phone1_extention varchar(10),
    phone1_display varchar(30),
    phone2 varchar(15),
    phone2_extention varchar(10),
    phone2_display varchar(30),
    phone3 varchar(15),
    phone3_extention varchar(10),
    phone3_display varchar(30),
    partner boolean,
    hire_date Date,
    leave_date Date
    );
    
create table customer (
    CID serial PRIMARY KEY,
    customer_name varchar(30)
);

create table customercontact (
    CCID serial PRIMARY KEY,
    CID integer REFERENCES customer(cid),
    first_name varchar(30),
    last_name varchar(30),
    email varchar(50),
    email2 varchar(50),
    phone1 varchar(15),
    phone1_extention varchar(10),
    phone1_display varchar(30),
    phone2 varchar(15),
    phone2_extention varchar(10),
    phone2_display varchar(30),
    phone3 varchar(15),
    phone3_extention varchar(10),
    phone3_display varchar(30)
);

create table project_type (
    pt serial PRIMARY KEY,
    project_type varchar(30)    
);

create table project (
    pid serial PRIMARY KEY,
    cid integer REFERENCES customer(cid),
    PT integer REFERENCES project_type(pt),
    project_name varchar(50),
    sales integer,
    resource_pri integer,
    resource_secondary integer,
    partner integer,
    sow_signed date,
    projected_start date,
    projected_end date,
    invoice_date date,
    invoiced boolean,
    CCID integer,
    status varchar(255)
);

create table project_notes (
    PNID serial PRIMARY KEY,
    PID integer REFERENCES project(pid),
    author integer,
    visible boolean,
    note varchar(255),
    date_entered date,
    date_deleted date
);