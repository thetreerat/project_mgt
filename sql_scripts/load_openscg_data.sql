\c project_board
insert into employee (last_name,
                      first_name,
                      email,
                      phone1,
                      phone1_extention,
                      phone1_display,
                      phone2,
                      phone2_extention,
                      phone2_display,
                      phone3,
                      phone3_extention,
                      phone3_display,
                      partner,
                      hire_date,
                      leave_date)
values
    ('Lussier','Denis','denisl@openscg.com',null,null,'',null,null,'', null,null,'', true, '2015/01/01', null),
    ('Mead','Scott','scottm@penscg.com','6077651395',null,'Mobile',null,null,'', null,null,'', true, '2015/01/01', null),
    ('Jim','Mlodgenski','jimm@penscg.com',null,null,'',null,null,'', null,null,'', true, '2015/01/01', null),
    ('Shaik','Baji','Baji.Shaik@penscg.com',null,null,'',null,null,'', null,null,'', false, '2015/01/01', null),
    ('Clark','Harold','harold.clark@penscg.com','5855124786',null,'Mobile','5856170204','117','Work',null,null,'', false, '2015/11/01', null),
    ('Kshirsagar','Swanand','swanand.kshirsagar@penscg.com',null,null,'',null,null,'', null,null,'', false, '2015/01/01', null),
    ('Muth','Candy','candy.muth@penscg.com',null,null,'',null,null,'', null,null,'', false, '2015/01/01', null),
    ('Nagavarapu','Raghav','vraghava.nagavarapu@openscg.com',null,null,'',null,null,'', null,null,'', false, '2015/01/01', null),
     ('Wieck','Jan','jan.wieck@openscg.com',null,null,'',null,null,'', null,null,'', false, '2016/03/16', null)
;

insert into project_type (project_type)
values
    ('AHC - Onsite'),
    ('AHC - Remote'),
    ('Migration'),
    ('DBA'),
    ('Other')
;
 
insert into customer (Customer_name)
values
    ('Nulogy'),
    ('Toast'),
    ('Navicure'),
    ('GE'),
    ('PointClick'),
    ('Fidelis')
;
insert into customercontact (CID,
                            first_name,
                            last_name,
                            email,
                            phone1,
                            phone1_extention,
                            phone1_display)
values
    ((select CID from customer where customer_name = 'Navicure'), 'David', 'Burn', 'dburn@navicare.com', '770-480-6999', null, 'Mobile'),
    ((select CID from customer where customer_name = 'Toast'), 'Max','Newell', 'MNewll@toasttab.com', null,null,null),
    ((select CID from customer where customer_name = 'Fidelis'), 'Gary','Crane', 'gary.crane@fideliscare.com', null,null,null),
    ((select CID from customer where customer_name = 'Nulogy'), 'Ian','Penney', null, null,null,null),
    ((select CID from customer where customer_name = 'PointClick'), 'Khola','Khan', null, null,null,null)
;
insert into project (cid,
                     pt,
                     project_name,
                     sales,
                     resource_pri,
                     resource_secondary,
                     sow_signed,
                     projected_start,
                     projected_end,
                     invoice_date,
                     invoiced,
                     CCID,
                     status)
values
    ((select CID from customer where customer_name = 'Toast'),
     (select pt from project_type where project_type='Migration'),
     'Toast Migration',
     null,
     (select eid from employee where employee.first_name='Baji'),
     (select eid from employee where employee.first_name='Swahand'),
     '02/01/20016',
     '02/11/2016',
     '03/03/2016',
     null,
     false,
     (select CCID from customercontact where first_name='Max'),
     null
     ),
     ((select CID from customer where customer_name = 'PointClick'),
     (select pt from project_type where project_type='Other'),
     'PointClick Project #1',
     null,
     (select eid from employee where employee.first_name='Raghav'),
     null,
     null,
     null,
     null,
     null,
     false,
     (select CCID from customercontact where first_name='Khola'),
     null
     ),
    
    ((select CID from customer where customer_name = 'GE'),
     (select pt from project_type where project_type='Migration'),
     'GE Big Fun Migration',
     null,
     (select eid from employee where employee.first_name='Jim'),
     null,
     null,
     null,
     null,
     null,
     false,
     null,
     null
     )
     ;